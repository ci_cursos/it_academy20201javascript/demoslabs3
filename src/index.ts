import * as fs from 'fs';

try {
    fs.writeFileSync('dados.txt','teste');
} catch (err) {
    console.log(err.name);
    console.log(err.message);
}

fs.writeFile('dados2.txt','teste', (err) => {
    if (err) {
        console.log(err.name);
        console.log(err.message);
    }
});
console.log('terminei');

async function testePromises() {
    try {
        let dados = await fs.promises.readFile('./dados.txt','utf-8');
        console.log('terminei');
        console.log(dados);
    } catch (err) {
        console.log(err);
    }
}

testePromises();

